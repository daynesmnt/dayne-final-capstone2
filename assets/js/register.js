let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()
	//console.log("Hello");
//get values from the form
	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

// validate user inputs//
	if((password1 !== "" && password2 !== "") && 
		(password1 === password2) &&
		(mobileNo.length == 11)){


		/* check if the database already has the email
	- fetch is a built-in JS function that allows getting of data from another source without the need to refresh a page
	- fetch send data to the URL provided with the following parametes 
		 method -> HTTP Method
		 header -> what kind of data to send
		 body -> the content of the req.body
	 */
		fetch("https://stormy-beyond-41410.herokuapp.com/api/users/email-exists",{
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			body : JSON.stringify({
				email : email
			})
		})// convert the response to json
		.then(response => response.json())
		// data is the json-converted response
		.then(data => {
			// alert(data)
			// if data is false, there there are no duplicates
			// if the data is true, there is a duplicate
			if(data === false) {
				// we fetch the registration route
				fetch("https://stormy-beyond-41410.herokuapp.com/api/users",{
					method : "POST",
					headers: {
						"Content-Type" : "application/json"
					},
					body : JSON.stringify({
						firstname : firstName,
						lastname: lastName,
						email : email,
						password : password1,
						mobileNo : mobileNo
					})
				})
				.then(response => response.json())
				.then(data => {
					// if data is true, registration is successful, else  something went wrong
					if (data) {
						alert("Registration successful")
						window.location.replace("./login.html")
					} else {
						alert("Registration went wrong")
					}
				})
			} else {
				alert("Email already exists")
			}
		}) 
	}
})