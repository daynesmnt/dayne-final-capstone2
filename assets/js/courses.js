let adminUser = localStorage.getItem("isAdmin");
let cardFooter ; //<- this will be used to add the button at the end of each card courses to go to a specific course
//console.log(adminUser)
let nonAdminElements = document.getElementsByClassName("nonAdmin");
let adminElements = document.getElementsByClassName("admin");
let elementsToRemove = [];
if ( adminUser == "false" || !adminUser) {
	elementsToRemove = adminElements;
} else {
	elementsToRemove = nonAdminElements;
}

for ( var i = 0; i < elementsToRemove.length; i++) {
		elementsToRemove[i].remove() ;
	}

console.log(adminUser)
fetch('https://stormy-beyond-41410.herokuapp.com/api/courses')
.then(res=> res.json())
.then(data => {
	//console.log(data)
	let courseData ;

	if(data.length < 1 ){
		courseData = " No courses available";
	} else {
		courseData = data.map(course => {

			//console.log(course._id)
			if(adminUser == "false" || !adminUser){
				cardFooter = `<a href = "./course.html?courseId=${course._id}"" value = ${course._id} class = "btn btn-primary text-white btn-block"> Go to Course </a>`
			} else {
				// checks if the user is an admin and if true create a button that deletes/archives the course
				fetch('https://stormy-beyond-41410.herokuapp.com/api/courses/allCourses')
				.then(res=> res.json())
				.then(data => {
	//console.log(data)
	let courseData ;
	for( var i=0 ; i < data.length; i++){
		let courseData = data[i];
		//console.log(courseData)


		if(data.length < 1 ){
		alert(" No courses available");
		
		} else {
		courseData = data.map(course => {
	//console.log(courseData.isActive)
			//console.log(courseData)
			if(course.isActive) {

			cardFooter = 
			`<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-danger text-white btn-block admin"> Archive Course </a>
			<a href = "./course.html?courseId=${course._id}"" value = ${course._id} class = "btn btn-primary text-white btn-block"> Go to Course </a>`



			} else{
			cardFooter = 
			`<a href="./enableCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-primary text-white btn-block admin"> Reactivate Course </a>
			<a href = "./course.html?courseId=${course._id}"" value = ${course._id} class = "btn btn-primary text-white btn-block"> Go to Course </a>`;

			}

			//console.log(course._id)
			return (
				`
					<div class = " col-md-6 my-3">
						<div class = "card">
							<div class = " card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									${course.price}
								</p>
							</div>
							<div class= "card-footer"> 
							${cardFooter}
							</div>
						</div>
					</div>
				`
			)
		}).join("")

	}
	let container = document.querySelector("#coursesContainer");
	container.innerHTML = courseData

	}
	})


			}
			return (
				`
					<div class = " col-md-6 my-3">
						<div class = "card">
							<div class = " card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									${course.price}
								</p>
							</div>
							<div class= "card-footer"> 
							${cardFooter}
							</div>
						</div>
					</div>
				`
			)
		}).join("")

	}


	let container = document.querySelector("#coursesContainer");
	container.innerHTML = courseData;
})