let params = new URLSearchParams(window.location.search)
courseId = params.get("courseId")
let token = localStorage.getItem("token") // cannot delete without admin log in


fetch(`https://stormy-beyond-41410.herokuapp.com/api/courses/enable/${courseId}`, {
			method : "POST",
			headers: {
				'Authorization' : `Bearer ${token}`
				// Token is placed in the authorization headers
			}
		})
	.then(res => {return res.json()})
			.then(data => {
				console.log(data)  // this would return only tru or false
				if (data){
					alert("You have reactivated this course")
					//window.location.replace("./courses.html")
				} else{
					alert("Reactivation failed")
				}
			})

