/*
STEPS:
Get the id from the url 
use the id as a parameter to the fetch and specify the url
process the data as needed
Return to the courses page


Validation is done to check if the entries in a form are correct
Authentication is done to check if the user is logged in por not
Authorization is done to check if athe user has privilages for certain actions.

*/

let params = new URLSearchParams(window.location.search)
courseId = params.get("courseId")
let token = localStorage.getItem("token") // cannot delete without admin log in


fetch(`https://stormy-beyond-41410.herokuapp.com/api/courses/${courseId}`, {
			method : "DELETE",
			headers: {
				'Authorization' : `Bearer ${token}`
				// Token is placed in the authorization headers
			}
		})
	.then(res => {return res.json()})
			.then(data => {
				console.log(data)  // this would return only tru or false
				if (data){
					alert("You have archived this course")
					//window.location.replace("./courses.html")
				} else{
					alert("Archive failed")
				}
			})

