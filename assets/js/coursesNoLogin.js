let adminUser = localStorage.getItem("isAdmin");
let cardFooter ; //<- this will be used to add the button at the end of each card courses to go to a specific course
//console.log(adminUser)

//if regular user

fetch('https://stormy-beyond-41410.herokuapp.com/api/courses')
.then(res=> res.json())
.then(data => {
	//console.log(data)
	let courseData ;

	if(data.length < 1 ){
		courseData = " No courses available";
	} else {
		courseData = data.map(course => {

			//console.log(course._id)
			return (
				`
					<div class = " col-md-6 my-3">
						<div class = "card">
							<div class = " card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									${course.price}
								</p>
							</div>
						</div>
					</div>
				`
			)
		}).join("")

	}

	let container = document.querySelector("#coursesContainer");
	container.innerHTML = courseData;
})
//if admin
